FROM golang:1.16-alpine3.13 AS builder
WORKDIR /go/src/gitlab.qleanlabs.ru/platform/infra/slack-first-line
COPY . .
RUN go build .

FROM alpine:3.13
COPY --from=builder /go/src/gitlab.qleanlabs.ru/platform/infra/slack-first-line/slack-first-line /
ENTRYPOINT [ "/slack-first-line" ]
