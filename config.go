package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"regexp"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
)

type Config struct {
	AppToken string  `json:"-" yaml:"-" envconfig:"APP_TOKEN" required:"true"`
	BotToken string  `json:"-" yaml:"-" envconfig:"BOT_TOKEN" required:"true"`
	Items    []*Item `json:"items" yaml:"items" ignored:"true"`
}

type Item struct {
	Name     string   `yaml:"name"`
	Match    string   `yaml:"match"`
	MatchAny []string `yaml:"matchAny"`
	Response string   `yaml:"response"`

	matchAnyRE []*regexp.Regexp
}

func LoadConfig(filename string) (*Config, error) {
	c := &Config{}
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(b, c)
	if err != nil {
		return nil, err
	}
	err = envconfig.Process("", c)
	if err != nil {
		return nil, err
	}
	return c, c.processItems()
}

func (c *Config) processItems() error {
	if len(c.Items) == 0 {
		return errors.New("empty item list")
	}
	for id, item := range c.Items {
		if item.Match != "" {
			item.MatchAny = append(item.MatchAny, item.Match)
		}
		if item.Response == "" {
			return fmt.Errorf("empty response for %d item", id)
		}
		for _, match := range item.MatchAny {
			re, err := regexp.Compile(match)
			if err != nil {
				return fmt.Errorf("failed to parse %q: %v", match, err)
			}
			item.matchAnyRE = append(item.matchAnyRE, re)
		}
	}
	return nil
}

func (c *Config) ItemByString(str string) (*Item, error) {
	for _, item := range c.Items {
		for _, matchRE := range item.matchAnyRE {
			if matchRE.MatchString(str) {
				return item, nil
			}
		}
	}
	return nil, errors.New("item not found")
}

func (c *Config) String() string {
	b, _ := json.Marshal(c)
	return string(b)
}
