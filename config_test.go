package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig_String(t *testing.T) {
	c := &Config{}
	c.processItems()
	assert.NotEmpty(t, c.String())
}

func TestLoadConfig(t *testing.T) {
	os.Unsetenv("APP_TOKEN")
	os.Unsetenv("BOT_TOKEN")

	_, err := LoadConfig("fixtures/valid.config.yaml")
	assert.Error(t, err)

	_, err = LoadConfig("fixtures/not-found.config.yaml")
	assert.Error(t, err)

	os.Setenv("APP_TOKEN", "AAA")
	os.Setenv("BOT_TOKEN", "BBB")

	_, err = LoadConfig("fixtures/empty-list.config.yaml")
	assert.Error(t, err)

	_, err = LoadConfig("fixtures/empty-response.config.yaml")
	assert.Error(t, err)

	_, err = LoadConfig("fixtures/incorrect-regexp.config.yaml")
	assert.Error(t, err)

	_, err = LoadConfig("fixtures/incorrect-syntax.config.yaml")
	assert.Error(t, err)

	_, err = LoadConfig("fixtures/valid.config.yaml")
	assert.NoError(t, err)
}

func TestConfig_ItemByString(t *testing.T) {
	config, err := LoadConfig("fixtures/valid.config.yaml")
	if assert.NoError(t, err) {
		item, err := config.ItemByString("foobar")
		assert.NoError(t, err)
		assert.NotNil(t, item)
		item, err = config.ItemByString("one")
		assert.NoError(t, err)
		assert.NotNil(t, item)
		item, err = config.ItemByString("barfoo")
		assert.Error(t, err)
		assert.Nil(t, item)
	}
}
