package main

import "github.com/sirupsen/logrus"

type slackLogAdapter struct {
	entry *logrus.Entry
}

func (a *slackLogAdapter) Write(b []byte) (int, error) {
	n := len(b)
	if n > 0 && b[n-1] == '\n' {
		b = b[:n-1]
	}
	a.entry.Info(string(b))
	return n, nil
}
