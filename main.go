package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"github.com/slack-go/slack/socketmode"

	"github.com/sirupsen/logrus"
)

var (
	configFile = flag.String("config", "", "Path to configuration file")
	debug      = flag.Bool("debug", false, "Enable debug mode")
)

func main() {
	flag.Parse()
	logrus.SetFormatter(&logrus.JSONFormatter{})

	if *debug {
		logrus.SetLevel(logrus.DebugLevel)
	}

	config, err := LoadConfig(*configFile)
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Infof("Configuration: %s", config.String())

	slackCli := slack.New(
		config.BotToken,
		slack.OptionAppLevelToken(config.AppToken),
		slack.OptionDebug(*debug),
		slack.OptionLog(log.New(&slackLogAdapter{
			entry: logrus.StandardLogger().WithField("source", "slack-go/slack:api"),
		}, "", 0)),
	)

	socketCli := socketmode.New(
		slackCli,
		socketmode.OptionDebug(*debug),
		socketmode.OptionLog(log.New(&slackLogAdapter{
			entry: logrus.StandardLogger().WithField("source", "slack-go/slack:socket"),
		}, "", 0)),
	)

	auth, err := slackCli.AuthTest()
	if err != nil {
		logrus.Fatal(err)
	}
	botID := auth.UserID

	logrus.Infof("Bot: %s", auth.User)

	go func() {
		err := socketCli.Run()
		if err != nil {
			logrus.Fatal(err)
		}
	}()

	go func() {
		for envelope := range socketCli.Events {
			switch envelope.Type {
			case socketmode.EventTypeHello:
				logrus.Debug("Hello")
			case socketmode.EventTypeConnected:
				logrus.Info("Connected")
			case socketmode.EventTypeEventsAPI:
				socketCli.Ack(*envelope.Request)
				eventPayload, ok := envelope.Data.(slackevents.EventsAPIEvent)
				if !ok {
					logrus.Warnf("Not an API event: %v", envelope.Data)
					continue
				}
				switch eventPayload.Type {
				case slackevents.CallbackEvent:
					switch event := eventPayload.InnerEvent.Data.(type) {
					case *slackevents.MessageEvent:
						if event.User == botID {
							continue
						}
						item, err := config.ItemByString(event.Text)
						if err != nil {
							logrus.WithField("user", event.User).
								Debugf("Response for %q not found", event.Text)
							continue
						}
						logrus.WithField("user", event.User).Info("Found response")
						_, _, err = slackCli.PostMessage(event.Channel,
							slack.MsgOptionText(item.Response, false),
							slack.MsgOptionTS(event.TimeStamp),
						)
						if err != nil {
							logrus.Error(err)
						}
					}
				}
			}
		}
	}()

	shutdownSignalChan := make(chan os.Signal, 1)
	signal.Notify(shutdownSignalChan, syscall.SIGINT, syscall.SIGTERM)

	<-shutdownSignalChan
	logrus.Info("Received shutdown signal")
}
